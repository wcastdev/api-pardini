@extends('layout.default')

@section('content')
    <div class="col-md-8 col-lg-12 order-md-1">
        <h4 class="mb-3">Teste Empresa Pardini - Gestor Paulo</h4>
        <hr class="mb-4">
        <div class="d-flex">
            <div class="col-md-12 mb-12">

                <h1>Teste</h1>

                <p>O objetivo do teste é avaliar seus conhecimentos com o framework Laravel, com a linguagem</p>
                
                <p>PHP e com GIT.</p>
                
                <p>Passos para execução do teste:</p>
                
                <p>1. Criar projeto no github ou bitbucket público</p>
                
                <p>2. Instalar o framework laravel</p>
                
                <p>3. Criar UMA tela no laravel usando blade que permita o cadastro dos seguintes campos:</p>
                
                <p>Nome, Sobrenome, Email, Data de Nascimento e até 6 Telefones e endereço (veja detalhes</p>
                <p>abaixo).</p>
                
                <p>4. Criar uma API no Laravel que retorne uma lista com os dados cadastrados no item</p>
                <p>anterior.</p>
                
                <p>5. Criar UMA tela que permita a visualização dos dados retornados pela API desenvolvida</p>
                <p>no item anterior em blade no formato de tabela.</p>
                
                <p>6. Criar uma tela que permita a visualização e edição do detalhe dos dados cadastrados, a</p>
                <p>forma que você deve recuperar os dados será através de um endpoint.</p>
                
                <p>7. Subir todos arquivos no repositório git (O projeto precisa ter um README com os</p>
                <p>passos para execução).</p>

                <p>O objetivo é que o avaliador possa ler o arquivo README e seguindo os passos faça com que</p>
                <p>o projeto seja executado.</p>
                
                <p>Detalhes:</p>
                <p>Para recuperar os dados do endereço, você deverá usar uma API pública que recupere os</p>
                <p>dados através do CEP. (​ https://viacep.com.br​ ) - Faça isso em Javascript.</p>

                <br/>

                <div class="col-md-12 mb-12 -align-right">
                    <a href="{{ route('contatos.index') }}">
                        <button type="button" class="btn btn-outline-dark">Cadastro de contatos</button>
                    </a>
                </div>

            </div>
        </div>
    </div>
@endsection

