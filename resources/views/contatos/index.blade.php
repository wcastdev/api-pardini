@extends('layout.default')

@section('content')
    <div class="col-md-12 col-lg-12">

        <h4 class="mb-3">Cadastros</h4>

        <hr class="mb-4">

        <div class="col-md-12 mb-12 -align-right">
            <a href="{{ route('contatos.create') }}">
                <button type="button" class="btn btn-outline-dark">Novo cadastro</button>
            </a>
        </div>

        <br/>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif

        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif


        <div class="d-flex">
            <div class="col-md-12 mb-12">
                @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
        </div>

        <br/>

        <div class="d-flex">
            <div class="col-md-12 mb-12">

                <div class="table-responsive">
                    <table class="table table-striped table-borderless">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Data de nascimento</th>
                            <th>Telefones</th>
                            <th>Editar</th>
                            <th>Excluir</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contatos as $contato)
                            <tr>
                                <td>{{$contato->id}}</td>
                                <td>{{$contato->nome}} {{$contato->sobrenome}}</td>
                                <td>{{$contato->email}}</td>
                                <td>{{$contato->data_nasc}}</td>
                                <td>
                                    @foreach($contato->telefones as $telefone)
                                    <p>{{$telefone->numero}}</p>
                                   @endforeach
                                </td>
                                <td>
                                    <a href="{{ route('contatos.edit', $contato->id) }}">
                                        <button class="btn">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>
                                    <form action="{{ route('contatos.destroy', $contato->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn" type="submit">
                                            <i style="color: red" class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

