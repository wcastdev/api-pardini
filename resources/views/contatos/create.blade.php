@extends('layout.default')

@section('content')
    <div class="col-md-8 col-lg-12 order-md-1">
        <h4 class="mb-3">Novo cadastro</h4>
        <hr class="mb-4">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif

        <form method="post" action="{{ route('contatos.store') }}">

            @csrf

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="firstName">Nome</label>
                    <input type="text" class="form-control" name="nome" placeholder="" value="" required>
                    <div class="invalid-feedback">
                        Informe o seu primeiro nome.
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="lastName">Sobrenome</label>
                    <input type="text" class="form-control" name="sobrenome" placeholder="" value="" required>
                    <div class="invalid-feedback">
                        Informe o seu sobrenome
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" placeholder="usuario@dominio.com">
                    <div class="invalid-feedback">
                        Informe um e-mail válido
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="address">Data de nascimento</label>
                    <input type="text" class="form-control" name="data_nasc" id="date_nasc" v-mask="'##/##/####'" placeholder="00/00/0000" required>
                    <div class="invalid-feedback">
                        Informe a sua data de nascimento.
                    </div>
                </div>
            </div>

            <span class="mb-4">

            <button class="btn btn-primary" type="submit">Salvar</button>
        </form>
    </div>
@endsection





