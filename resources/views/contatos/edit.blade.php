@extends('layout.default')

@section('content')
    <div class="col-md-8 col-lg-12 order-md-1">

        <h4 class="mb-3">Novo cadastro</h4>

        <hr class="mb-4">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif

        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif

        <form class="needs-validation" novalidate="" action="{{ route('contatos.update', $contato->id) }}"
              method="POST">

            {{ method_field('PUT') }}

            @csrf

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="firstName">Nome</label>
                    <input type="text" class="form-control" placeholder="" name="nome" value="{{$contato->nome}}"
                           required>
                    <div class="invalid-feedback">
                        Informe o seu primeiro nome.
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="lastName">Sobrenome</label>
                    <input type="text" class="form-control" placeholder="" name="sobrenome"
                           value="{{$contato->sobrenome}}" required>
                    <div class="invalid-feedback">
                        Informe o seu sobrenome
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" value="{{$contato->email}}"
                           placeholder="usuario@dominio.com" required>
                    <div class="invalid-feedback">
                        Informe um e-mail válido
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="address">Data de nascimento</label>
                    <input type="text" class="form-control" name="data_nasc" id="data_nasc"
                           value="{{$contato->data_nasc}}" placeholder="00/00/0000" required>
                    <div class="invalid-feedback">
                        Informe a sua data de nascimento.
                    </div>
                </div>
            </div>

            <span class="mb-4"></span>

            <button class="btn btn-primary pull-right" type="submit">Atualizar</button>

        </form>

        <h4>Telefomes / Endereços</h4>

        @if (count($contato->telefones ) <= 5)

         <hr class="mb-4">

        <form class="needs-validation" novalidate="" action="{{ route('telefones.store') }}" method="POST">
            @csrf
            <div class="row">

                <div class="col-md-2 mb-1">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" placeholder="00000-00" v-model="cep" v-mask="'#####-###'"  id="cep" name="cep" value="" required>
                </div>

                <div class="col-md-7 mb-4">
                    <label for="endereco">Endereço</label>
                    <input type="text" class="form-control" placeholder="" id="endereco" v-model="endereco" name="endereco" value="" required>
                </div>

                <div class="col-md-3 mb-2">
                    <label for="telefone">Telefone</label>
                    <input type="text" class="form-control" placeholder="(00) 0000-0000" id="date_nasc" v-mask="'(##) ####-####'" name="numero" value="" required>
                </div>

                <input name="contato_id" type="hidden" value="{{$contato->id}}">
            </div>
            <span class="mb-4"></span>
            <button class="btn btn-primary pull-right" type="submit">Adicionar</button>
        </form>
        <br/>
        <br/>
        @endif

        <hr class="mb-4">

        <div class="row">
            <div class="col-md-12 mb-12">
                <table class="table table-striped table-borderless">
                    <thead>
                    <tr>
                        <th width="170">Telefone</th>
                        <th>Endereço</th>
                        <th  width="170">CEP</th>
                        <th width="10">Excluir</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contato->telefones as $telefone)
                        <tr>
                            <td>{{$telefone->numero}}</td>
                            <td>{{$telefone->endereco}}</td>
                            <td>{{$telefone->cep}}</td>
                            <td>
                                <form action="{{ route('telefones.destroy', $telefone->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn" type="submit">
                                        <i style="color: red" class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>
@endsection





