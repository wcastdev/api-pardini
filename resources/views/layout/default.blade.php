<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/app.css">
    <title>API - Pardini</title>
</head>
<body>
@include('includes.navbar')
<div class="container-fluid" id="app">
    <div class="row">
        @include('includes.sidebar')

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-3 m-2">
            @yield('content')
        </main>
    </div>
</div>
<script type="text/javascript" src="/js/app.js"></script>
</body>
</html>