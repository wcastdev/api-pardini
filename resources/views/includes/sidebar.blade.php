<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="{{route('contatos.index')}}">
                    <span data-feather="file"></span>
                    Cadastros
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('contatos.create')}}">
                    <span data-feather="shopping-cart"></span>
                    Novo cadastro
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/sobre">
                    <span data-feather="users"></span>
                    Desenvolvedor
                </a>
            </li>
        </ul>
    </div>
</nav>
