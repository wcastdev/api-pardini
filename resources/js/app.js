require("./bootstrap")
window.$ = require('jquery')
window.JQuery = require('jquery')
window.Vue = require("vue")
import Vue from 'vue'
import VueTheMask from 'vue-the-mask'
import ApiUsers from './components/api-users'
Vue.use(VueTheMask)
new Vue({
    el: '#app',
    components: {
        ApiUsers
    },
    data: {
        title: 'Pardini',
        endereco: '',
        cep: ''
    },
    watch: {
        cep: {
            handler: function (val) {
                if(val.length >= 9){
                    this.getCep(val)
                }
            },
            deep: true
        }
    },
    methods: {
        getCep(cep) {
            var self = this
            axios({
                method: 'get',
                url: '/api/get-cep/'+cep,
            }).then(function (response) {
                self.endereco = response.data;
            });
        }
    }
})