# API LARAVEL

### Requisitos de sistema 

- Apache Server

- PHP >= 7.2

- MySQL Server 5.7


### Instalar o Laravel e suas depenências

```
$ composer install

```

### Criar banco de dados, usuário e conceder permissões

```
$ sudo mysql;

$ mysql> use mysql;

$ mysql> CREATE DATABASE db_pardini CHARACTER SET utf8 COLLATE utf8_general_ci;

$ mysql> CREATE USER 'pardini'@'localhost' IDENTIFIED BY 'api!@123';

$ mysql> GRANT ALL PRIVILEGES ON db_pardini.* TO pardini@localhost IDENTIFIED BY 'api!@123';

$ mysql> FLUSH PRIVILEGES;

$ mysql> exit;

```

### Configurar o ambiente Laravel

```

APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:6cgQC9ZIsl90I469cGBhDpQv5cPA5D62b+nZ0c5/VnI=
APP_DEBUG=true
APP_URL=http://127.0.0.1:8000

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_pardini
DB_USERNAME=pardini
DB_PASSWORD=api!@123

```

### Executar as migrations para subir o banco

```
php artisan migrate

```

### Executar a aplicação usando o artisan

```

$ php artisan serve


```

## Observações

A requisição para a consulta do CEP é realizada com o Vue JS.








