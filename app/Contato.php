<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Contato extends Model
{
    use Notifiable;

    protected $fillable = [];

    protected $hidden = [];

    protected $guarded = [];

    public function telefones(){

        return $this->hasMany('App\Telefone');
    }
}
