<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Telefone extends Model
{
    use Notifiable;

    protected $fillable = [];

    protected $hidden = [];

    protected $guarded = [];
}
