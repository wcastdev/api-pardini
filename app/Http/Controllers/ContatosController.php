<?php

namespace App\Http\Controllers;

use App\Contato;
use App\Telefone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ContatosController extends Controller
{

    public function saveDateTime($data)
    {
        if ($data != '') {
            return implode("-", array_reverse(explode("/", $data)));
        } else {
            return '0000-00-00';
        }
    }

    public function getCep($cep)
    {
        $cep = str_replace('.', '', $cep);
        $cep = str_replace('-', '', $cep);
        $reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=txt&cep=" . $cep);
        $dados['success'] = (string)$reg->resultado;
        $endereco = (string)$reg->tipo_logradouro . ' ' . $reg->logradouro.',  '.(string)$reg->bairro.' '.(string)$reg->cidade.'-'.(string)$reg->uf;
        return $endereco;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contatos.index')->with('contatos', Contato::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('contatos.create');
    }

    /**
     * salvar.
    */

    public function store(Request $request)
    {
        $data = $request->all();

        $data['data_nasc'] = $this->saveDateTime($data['data_nasc']);

        $validator = Validator::make($data,[
            'nome' => 'required|max:100',
            'sobrenome' => 'required|max:100',
            'data_nasc' => 'required|date',
            'email' => 'required|max:255|unique:contatos'
        ]);

        if ($validator->fails()) {
            return redirect()->route('contatos.create')
                ->withErrors($validator)
                ->withInput();
        }

        Contato::create($data);
        $request->session()->flash('success', 'Registro adicionado com sucesso!');
        return redirect()->route('contatos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       return view('contatos.edit')->with(['contato' => Contato::findOrFail($id), 'telefones' => Telefone::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $contato = Contato::findOrFail($id);

        $data = $request->all();

        $data['data_nasc'] = $this->saveDateTime($data['data_nasc']);

        $validator = Validator::make($data,[
            'nome' => 'required|max:100',
            'sobrenome' => 'required|max:100',
            'data_nasc' => 'required|date',
            'email' => [
                'required',
                Rule::unique('users')->ignore($contato->id),
            ]
        ]);

        if ($validator->fails()) {
            return redirect()->route('contatos.create')
                ->withErrors($validator)
                ->withInput();
        }

        $contato->fill($data)->save();
        $request->session()->flash('success', 'Registro atualizado com sucesso!');
        return redirect()->route('contatos.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request,$id)
    {
        $contato = Contato::find($id);
        $contato->delete();
        $request->session()->flash('success', 'Contato excluído com sucesso!');
        return redirect()->back();
    }
}
