<?php

namespace App\Http\Controllers;

use App\Telefone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use mysql_xdevapi\Session;

class TelefonesController extends Controller
{

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data,[
            'numero' => 'required|max:100',
            'endereco' => 'required|max:400',
            'cep' => 'required|max:10',
        ]);

        if ($validator->fails()) {
            return redirect()->route('contatos.edit',$data['contato_id'])
                ->withErrors($validator)
                ->withInput();
        }

        Telefone::create($data);
        $request->session()->flash('success', 'Registro adicionado com sucesso!');
        return redirect()->back();
    }


    public function destroy(Request $request,$id)
    {
        $telefone = Telefone::find($id);
        $telefone->delete();
        $request->session()->flash('success', 'Registro excluído com sucesso!');
        return redirect()->back();
    }
}
